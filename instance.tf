# set IP tĩnh cho instance 
resource "google_compute_address" "static-ip-address-demo" {
  count = "${var.node_count}"
  #  count = 1
  name = "${format("demo-%02d", count.index + 1)}"
}
# build instance from snapshot
resource "google_compute_disk" "demo" {
  count = "${var.node_count}"
  #  count = 1
  name  = "${format("demo-%02d", count.index + 1)}"
  type  = "${var.type_disk}"
  image = "${var.instance_os}" #centos-7-v20190619"
  zone  = "${var.region}"
  # snapshot = "${var.snapshot_image}"  
  size = 10
  timeouts {
    create = "60m"
    delete = "2h"
  }

}
# define disk cho instance and attack disk

# định nghĩa cho 1 instance
resource "google_compute_instance" "demo_demo" {
  count = "${var.node_count}"
  name  = "${format("demo-%02d", count.index + 1)}"
  #tags = ["http","game"]
  allow_stopping_for_update = true
  machine_type              = "${var.machine}"
  zone                      = "${var.region}"
  boot_disk {
    device_name = "${element(google_compute_disk.demo.*.name, count.index)}"
    source      = "${element(google_compute_disk.demo.*.self_link, count.index)}"
    auto_delete = false
  }
  network_interface {
    network = "default"
    access_config {
      nat_ip = "${element(google_compute_address.static-ip-address-demo.*.address, count.index)}"
    }
  }

}
