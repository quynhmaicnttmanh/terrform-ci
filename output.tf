output "IP_PUBLIC" {
  value = "${google_compute_instance.demo_demo.*.network_interface.0.access_config.0.nat_ip}"
}
