variable "node_count" {
  description = "number instance"
  # type    = "list" 
  default = "1"
}
variable "region" {
  default = "asia-east1-a"
}
variable "type_disk" {
  default = "pd-standard"
}
#g1-small f1-micro
variable "machine" {
  default = "f1-micro"
}
variable "snapshot_image" {
  default = "game"
}
#centos-6-v20190729;centos-7-v20190729
variable "instance_os" {
  default = "centos-7-v20190729"
}
