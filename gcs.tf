terraform {
  backend "gcs" {
    bucket = "demo-terraform"
    prefix = "terraform"
    credentials = "./gcp/m82mm.json"
  }
}
